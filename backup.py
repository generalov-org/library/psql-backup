import os
import logging
from datetime import datetime

import boto
from boto.s3.key import Key


def upload_to_s3(filename):
  logging.info(f"upload_to_s3({filename})")
  conn = boto.connect_s3(
    aws_access_key_id = os.getenv('AWS_ACCESS_KEY_ID'),
    aws_secret_access_key = os.getenv('AWS_SECRET_ACCESS_KEY'),
    host = os.getenv('AWS_HOST'),
    port = int(os.getenv('AWS_PORT'))
  )
  logging.info("conn.get_bucket")
  bucket = conn.get_bucket(os.getenv('AWS_BUCKET_NAME'))
  k = Key(bucket)
  k.key = filename
  logging.info("set_contents_from_filename")
  k.set_contents_from_filename(filename)
  logging.info("uploaded")

if __name__ == '__main__':
  source_path = os.getenv('BASE_PATH')
  filename = source_path + datetime.strftime(datetime.now(), "%Y.%m.%d.%H:%M") + 'UTC' + '.sql'
  for command in [
    'mkdir -p ' + source_path,
    'pg_dump -h $PG_HOST -U $PG_USER $PG_DATABASE > ' + filename
  ]:
    logging.info(f"executing {command}")
    exit_code = os.system(command)
    if exit_code != 0:
      raise Exception(f"exit_code {exit_code} != 0")
  upload_to_s3(filename)
