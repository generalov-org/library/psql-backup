# psql-backup

# psql-backup

Image for psql db dump in k8s

Secret:
```
apiVersion: v1
kind: Secret
metadata:
  name: pgdumpersecret
type: Opaque
stringData:
    PG_HOST: "database-host"
    PG_PASSWORD: "pgpass"
    PG_USER: "pguser"
    PG_DATABASE: "db"
    AWS_ACCESS_KEY_ID: "xxxxxxxxxxxxxxxx"
    AWS_SECRET_ACCESS_KEY: "yyyyyyyyyyyyyyyyyyyyyyyyyy"
    AWS_BUCKET_NAME: "maindbbackup"
    AWS_HOST: "do"
    AWS_PORT: "443"
```
CronJob:
```
apiVersion: batch/v1beta1
kind: CronJob
metadata:
  name: pgdumper
spec:
  schedule: "0 */4 * * *"
  successfulJobsHistoryLimit: 2
  concurrencyPolicy: Replace
  jobTemplate:
    spec:
      template:
        spec:
          restartPolicy: OnFailure
          containers:
          - name: pgdumper
            image: registry.proksy.io/sysadmin/psql-backup:latest
            imagePullPolicy: Always
            env:
              - name: PG_HOST
                valueFrom:
                   secretKeyRef:
                      name: pgdumpersecret
                      key: PG_HOST
              - name: PGPASSWORD
                valueFrom:
                   secretKeyRef:
                      name: pgdumpersecret
                      key: PGPASSWORD
              - name: PG_USER
                valueFrom:
                   secretKeyRef:
                      name: pgdumpersecret
                      key: PG_USER
              - name: PG_DATABASE
                valueFrom:
                   secretKeyRef:
                      name: pgdumpersecret
                      key: PG_DATABASE
              - name: AWS_ACCESS_KEY_ID
                valueFrom:
                   secretKeyRef:
                      name: pgdumpersecret
                      key: AWS_ACCESS_KEY_ID
              - name: AWS_SECRET_ACCESS_KEY
                valueFrom:
                   secretKeyRef:
                      name: pgdumpersecret
                      key: AWS_SECRET_ACCESS_KEY
              - name: AWS_BUCKET_NAME
                valueFrom:
                   secretKeyRef:
                      name: pgdumpersecret
                      key: AWS_BUCKET_NAME
              - name: AWS_HOST
                valueFrom:
                   secretKeyRef:
                      name: pgdumpersecret
                      key: AWS_HOST
              - name: AWS_PORT
                valueFrom:
                   secretKeyRef:
                      name: pgdumpersecret
                      key: AWS_PORT
```
