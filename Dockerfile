FROM python:3-alpine
RUN apk add --no-cache postgresql
RUN pip install --no-cache boto
COPY backup.py .
CMD python backup.py
